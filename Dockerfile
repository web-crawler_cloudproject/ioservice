FROM node:14-alpine

WORKDIR /app

COPY package*.json ./

RUN ["npm", "install"]

RUN ["npm", "ci"]

COPY . .

EXPOSE 3002

CMD ["node", "server.js"]
